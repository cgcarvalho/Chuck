const urlApi = "http://api.icndb.com/";
const quantidade = document.querySelector("#quantidade");
const mensagem = document.querySelector("#mensagem");

function extrairJSON(resposta) {
    return resposta.json();
}

function buscarFato() {
    mensagem.innerHTML = "";
    let urlBusca = `${urlApi}jokes/random/${quantidade.value}/`;
    if(categorias.value !== "todas") {
        urlBusca = `${urlBusca}?limitTo=${categorias.value}`;
    }

    fetch(urlBusca)
        .then(extrairJSON)
        .then(exibirMensagem);
}


function preencherOpcoes(dados) {
    console.log(JSON.stringify(dados));
    let opcoes = "<option value=\"todas\">todas</option>";
    for (const opcao of dados.value) {
        opcoes = opcoes + `<option value=\"${opcao}\">${opcao}</option>`;
    }
    categorias.innerHTML = opcoes;
}

function exibirMensagem(dados) {
    for(let dado of dados.value){
        adicionarMensagem(dado);
    }
}

function adicionarMensagem(fato){
    let paragrafoFato = document.createElement("p");
    paragrafoFato.innerHTML = fato.joke;
    mensagem.appendChild(paragrafoFato);
}

function buscarOpcoes() {
    fetch(`${urlApi}categories`)
        .then(extrairJSON)
        .then(preencherOpcoes);
}

buscar.onclick = buscarFato;
window.onload = buscarOpcoes;